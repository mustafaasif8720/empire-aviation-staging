<?php get_header(); ?>

<?php /* Template Name: Blog */ 
?>

<header class="innerheader animated fadeInLeft" style="background: url(<?php the_post_thumbnail_url(); ?>);">
	<div class="overlay">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="innercontent">
						<h2><?php the_title(); ?></h2>
						<h3><?php the_field('sub_title'); ?></h3>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
<div class="container">
		<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; ?>

<div class="pageContent camo">	
	
		
		<!--List-->
		<div class="row clearBoth">
			<?php
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$args = array(  'category_name' => 'blog', 'category' => '4', 'post_type' => 'post', 'posts_per_page' => 6, 'orderby' => 'DESC', 'paged' => $paged );
				$loop = new WP_Query( $args );
				while ( $loop->have_posts() ) : $loop->the_post();;
			?>
			<div class="col-xl-4">
				<div class="newsarea">
					<?php if(has_post_thumbnail()=="") { ?>
						<span class="ThumbnailImg" style="background: url('https://empireaviation.com/wp-content/uploads/2021/03/default-news2.jpg');"></span>
					<?php } else { ?>
						<span class="ThumbnailImg" style="background: url(<?php the_post_thumbnail_url(); ?>);"></span>
					<?php } ?>
					<span class="date"><?php echo get_the_date(); ?></span>
					<h4><?php echo wp_trim_words (get_the_title(), 10, '...' ); ?></h4>
					<p><?php the_excerpt(); ?></p>
					<a href="<?php the_permalink(); ?>" class="">Read More</a>
				</div>
			</div>
			<?php endwhile; wp_reset_query(); ?>
		</div>
		<!--Pagination-->
		<div class="row">
			<div class="col-xl-12 text-right">
				<nav class="pagination">
			    <?php
						if( $loop->max_num_pages > 1 ){
					?>
			      	<span class="page-label">Pages:</span>
					<?php
					}
					
					?>
			      	<?php
			        	$big = 999999999;
			        	echo paginate_links( array(
			            'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
			            'format' => '?paged=%#%',
			            'current' => max( 1, get_query_var('paged') ),
			            'total' => $loop->max_num_pages,
			            'prev_text' => 'Pages:',
			            'next_text' => '&raquo;'
			        	));
			        ?> 
			    </nav>
			</div>
		</div>
	</div>
</div>

	

<?php get_footer(); ?>