<?php get_header(); ?>
<?php /* Template Name: About Us */ ?>

<header class="innerheader animated fadeInLeft" style="background: url(<?php the_post_thumbnail_url(); ?>);">
	<div class="overlay">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="innercontent">
						<h2><?php the_title(); ?></h2>
						<h3><?php the_field('sub_title'); ?></h3>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>

<div class="pageContent camo">	
	<div class="container">
		<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; // end of the loop. ?>
	</div>
</div>


<div class="timelinewrap" style="padding-top: 0;">
	<div class="container">
		<h3>Empire Aviation</h3>
		<h2>our story</h2>
		<div class="timeline">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="timeline-container">
							<div class="timeline-end">
								<p>&nbsp;</p>
							</div>
							
							<div class="timeline-continue">
								<?php
									$args = array( 'post_type' => 'story', 'posts_per_page' => -1,'orderby' => 'DESC' );
									$loop = new WP_Query( $args );
									while ( $loop->have_posts() ) : $loop->the_post(); global $product;
								?>
								<div class="row timeline-right">
									<div class="col-md-6 bulit-Point">
										<p class="timeline-date">&nbsp;</p>
									</div>
									<div class="col-md-6 contentSection">
										<div class="timeline-box">

											<div class="timeline-text">
												<h2><?php the_field('date'); ?></h2>
												<h3><?php the_title(); ?></h3>
												<?php the_content(); ?>
												<br>
												<img src="<?php the_post_thumbnail_url(); ?>" alt="" />
											</div>
										</div>
									</div>
								</div>
								<?php endwhile; wp_reset_query(); ?>
								
							</div>
							
							<div class="timeline-start">
								<p>&nbsp;</p>
							</div>
							<br /><br />
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<?php get_footer(); ?>