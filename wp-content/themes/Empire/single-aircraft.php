<?php get_header(); ?>

<header class="innerheader animated fadeInLeft slide-aircraftsale-bg5000">
    <div class="overlay">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="innercontent">
                        <h2><?php the_title(); ?></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="container">

	<!--Gallery-->
    <div class="camo">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <div class="row">
                	<?php if (have_rows('gallery')): ?>
					<?php while (have_rows('gallery')) : the_row(); ?>
                        <div class="col-md-4 mb-4">
                            <a class="fancybox" href="<?php the_sub_field('image'); ?>"
                                data-fancybox-group="gallery" title="<?php the_title(); ?>"><img src="<?php the_sub_field('image'); ?>" alt="" />
                            </a>
                        </div>
                    <?php endwhile; ?>
					<?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <!--Details-->
    <div class="row mb-5">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a data-toggle="tab" href="#airframe" class="nav-link active">Airframe</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#engine" class="nav-link">Engine</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#avionics" class="nav-link">Avionics</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#interior" class="nav-link">Interior</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#exterior" class="nav-link">Exterior</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#additional" class="nav-link">Additional Info</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="airframe">
                    <p><?php the_field('airframe'); ?></p>
                </div>
                <div class="tab-pane fade" id="engine">
                    <p><?php the_field('engine'); ?></p>
                </div>
                <div class="tab-pane fade" id="avionics">
                    <p><?php the_field('avionics'); ?></p>
                </div>
                <div class="tab-pane fade" id="interior">
                    <p><?php the_field('interior'); ?></p>
                </div>
                <div class="tab-pane fade" id="exterior">
                    <p><?php the_field('exterior'); ?></p>
                </div>
                <div class="tab-pane fade" id="additional">
                    <p><?php the_field('additional_info'); ?></p>
                </div>
            </div>
        </div>
    </div>

    <!--Others-->
    <div class="row my-5">
        <div class="col-12 text-center">
            <h3>Other Aircraft For Sale</h3>
        </div>
        
        <?php
	    $args = array(
	      	'post_type' => 'aircraft',
	      	'posts_per_page' => 3,
	      	'post__not_in' => array( $post->ID )
	    );
	    	$loop = new WP_Query( $args );
	    	while ( $loop->have_posts() ) : $loop->the_post();
	    ?>
        <div class="col-xl-4 col-lg-4 col-md-4 col-xs-12">
            <div class="img-wrap">
                <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="my-3">
                <?php if (get_field('sold') != "") { ?>
                    <span class="img-label">Sold</span>
                <?php } else { ?>
                <?php } ?>
            </div>
            <?php if (get_field('sold') != "") { ?>
                <a class="other-title" href="javascript:void(0)" class="py-3 my-3" style="opacity:.5"><?php the_title(); ?></a>
            <?php } else { ?>
                <a class="other-title" href="<?php the_permalink(); ?>" class="py-3 my-3"><?php the_title(); ?></a>
            <?php } ?>
            <br>
            <p><?php the_content(); ?></p>
        </div>
        <?php endwhile; ?>
    </div>
</div>

<style>
.img-wrap {
    position: relative;
}
.img-label {
    position: absolute;
    color: #d8ad19;
    font-size: 20px;
    font-weight: bold;
    text-transform: uppercase;
    width: 27%;
    top: 15px;
    left: 10px;
}
</style>

<?php get_footer(); ?>