<?php get_header(); ?>

<?php
/*
 * Template Name: Tabs
 * Template Post Type: vacations
 */
  
 get_header();  ?>

<header class="innerheader animated fadeInLeft soneva-kiri private-plane ">
	<video autoplay muted loop id="bgVideo">
		<source src="<?php the_field('video'); ?>" type="video/mp4">
	</video>
	
	<div class="container">
	  <div class="sub-container">
		<div class="row">
		  <div class="col-lg-12">
			<div class="innercontent">
			</div>
		  </div>
		</div>
	  </div>
	</div>
</header>




<div class="container private-plane">

    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-12 pt-4 text-center luxury-panel animated zoomIn private-plane">
        <div class="row">
		
          <ul class="nav nav-tabs justify-content-center MyTabs">

<?php
	$i=1;
	if( have_rows('add_tab') ):
	while ( have_rows('add_tab') ) : the_row();
		if( $i<10):
?>
	<li><a data-toggle="tab" class="custombtn btn-outline" href="#Ctab<?php echo $i; ?>"><?php the_sub_field('tab_title'); ?></a></li>
<?php $i++;  endif; endwhile; endif; ?>  
            <!--<li><a data-toggle="tab" class="custombtn btn-outline" href="#lemancha">La Mancha Estate </a></li>
            <li><a data-toggle="tab" class="custombtn btn-outline" href="#heseen">Yacht Charter </a></li>-->
          </ul>

          <div class="tab-content MyTabs-content mb-4">
<?php
	$i=1;
	if( have_rows('add_tab') ):
	while ( have_rows('add_tab') ) : the_row();
		if( $i<10):
?>
            <div id="Ctab<?php echo $i; ?>" class="tab-pane fade">
              <div class="col-md-12 col-12">
				<?php if( get_sub_field('main_title') ): ?>
					<h3 class="text-center"><?php the_sub_field('main_title'); ?></h3>
				<?php endif; ?>
				
				<?php if( get_sub_field('sub_title') ): ?>
					<h2 class="text-center"><?php the_sub_field('sub_title'); ?></h2>
				<?php endif; ?>
				
                <div class="row mt-5">
                  <div class="col-md-6">
                    <?php the_sub_field('content'); ?>
                  </div>
                  <div class="col-md-6 text-left">
                    <div id="videopopup">
                      <a href="<?php the_sub_field('video'); ?>" title=""><img src="<?php the_sub_field('video_poster'); ?>" alt="" /></a>
                    </div>
                  </div>
                </div>
				
              </div>
			  
			<?php
				if( have_rows('gallery') ):
				while ( have_rows('gallery') ) : the_row();		
			?>
              <div class="row my-5 imageswipe fi d-none d-md-flex">
                <div class="col-md-6 mb-md-0 mb-4">
                  <a class="fancybox" href="<?php the_sub_field('add_image'); ?>" data-fancybox-group="togomogo" title="">
					<img src="<?php the_sub_field('add_image'); ?>" alt="" /></a>
                </div>
                <div class="col-md-6">
                  <div class="row collage-photos">
                    <div class="col-md-6 mb-4">
                      <a class="fancybox" href="<?php the_sub_field('add_image_2'); ?>" data-fancybox-group="togomogo"
                        title=""><img src="<?php the_sub_field('add_image_2'); ?>" alt="" /></a>
                    </div>
                    <div class="col-md-6 mb-md-0 mb-4">
                      <a class="fancybox" href="<?php the_sub_field('add_image_3'); ?>" data-fancybox-group="togomogo"
                        title=""><img src="<?php the_sub_field('add_image_3'); ?>" alt="" /></a>
                    </div>
                    <div class="col-md-6 mb-md-0 mb-4">
                      <a class="fancybox" href="<?php the_sub_field('add_image_4'); ?>" data-fancybox-group="togomogo"
                        title=""><img src="<?php the_sub_field('add_image_4'); ?>" alt="" /></a>
                    </div>
                    <div class="col-md-6 mb-md-0 mb-4">
                      <a class="fancybox" href="<?php the_sub_field('add_image_5'); ?>" data-fancybox-group="togomogo"
                        title=""><img src="<?php the_sub_field('add_image_5'); ?>" alt="" /></a>
                    </div>
                  </div>
                </div>
              </div>
			<?php endwhile; endif; ?>

              <div class="row my-5 imageswipe animated d-block d-md-none zoomIn">
                <div class="col-12 gallery-slick">
				<?php
					if( have_rows('gallery') ):
					while ( have_rows('gallery') ) : the_row();		
				?>
                  <div><img src="<?php the_sub_field('add_image'); ?>" alt="" /></div>
                  <div><img src="<?php the_sub_field('add_image_2'); ?>" alt="" /></div>
                  <div><img src="<?php the_sub_field('add_image_3'); ?>" alt="" /></div>
                  <div><img src="<?php the_sub_field('add_image_4'); ?>" alt="" /></div>
                  <div><img src="<?php the_sub_field('add_image_5'); ?>" alt="" /></div>
				<?php endwhile; endif; ?>
                </div>
              </div>
              <div class="onlineform luxury-panel">
                <h3 class="text-center">Fill the below form for information</h3>
                <h2><?php the_sub_field('tab_form_title'); ?></h2>
                <?php echo do_shortcode('[contact-form-7 id="392" title="Single Post"]'); ?>

              </div>
            </div>
			
<?php $i++;  endif; endwhile; endif; ?>

          </div>
        </div>


      </div>

    </div>
</div>




<?php get_footer(); ?>
<script>
	jQuery(".MyTabs li:nth-child(1)").addClass("active");
	jQuery(".MyTabs li:nth-child(1) a").addClass("active");
	
	jQuery(".MyTabs-content .tab-pane:nth-child(1)").addClass("active");
	jQuery(".MyTabs-content .tab-pane:nth-child(1)").addClass("show");
	
	$(function () {
		$('#videopopup a').fancybox({
		  width: 640,
		  height: 360,
		  type: 'iframe'
		});
	  });
</script>
