<?php get_header(); ?>

<header class="innerheader animated fadeInLeft" style="background: url(https://empireaviation.com/wp-content/uploads/2021/02/news-1.jpg);">
	<div class="overlay">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="innercontent">
						<h2>News</h2>
						<h3>All the latest updates</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>

<div class="pageContent camo">	
	<div class="container">
		<!--Archive-->
		<ul class="year-archive">
			<li>Archive:</li>
			<?php 
				$args = array(
				    'type'            => 'yearly',
				    'limit'           => '',
				    'format'          => 'html', 
				    'before'          => '<div style="display:contents;"class="yearly">',
				    'after'           => '</div>',
				    'show_post_count' => false,
				    'echo'            => 1,
				    'order'           => 'DESC',
				);
				wp_get_archives( $args );
			?>
		</ul>
		<!--List-->
		<div class="row clearBoth">
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
			<div class="col-xl-4">
				<div class="newsarea">
					<span class="ThumbnailImg" style="background: url(<?php the_post_thumbnail_url(); ?>);"></span>
					<span class="date"><?php echo get_the_date(); ?></span>
					<h4><?php echo wp_trim_words (get_the_title(), 10, '...' ); ?></h4>
					<p><?php the_excerpt(); ?></p>
					<a href="<?php the_permalink(); ?>" class="">Read More</a>
				</div>
			</div>
			<?php endwhile; ?>
		</div>
		<!--Pagination-->
		<div class="row">
			<div class="col-xl-12 text-right">
				<nav class="pagination">
			      	<?php
			        	$big = 999999999;
			        	echo paginate_links( array(
			            'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
			            'format' => '?paged=%#%',
			            'current' => max( 1, get_query_var('paged') ),
			            'total' => $loop->max_num_pages,
			            'prev_text' => '&laquo;',
			            'next_text' => '&raquo;'
			        	));
			        ?> 
			    </nav>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>