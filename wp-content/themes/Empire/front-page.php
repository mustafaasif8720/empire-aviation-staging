<?php get_header(); ?>

<header class="home-slide animated fadeInLeft ">
    <div class="slider slider-for zoom mainSlider">
		<?php
			$args = array( 'post_type' => 'slider', 'posts_per_page' => -1,'orderby' => 'DESC' );
				$loop = new WP_Query( $args );
				while ( $loop->have_posts() ) : $loop->the_post(); global $product;
		?>
      <div style="background: url(<?php the_post_thumbnail_url(); ?>;">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="SliderContent">
                <!-- <h2>Welcome to Empire Aviation</h2> -->
                <!-- <h3 class="text-center">Buying an aircraft is a serious investment
                  commitment and a buyer should have the
                  right to the best specialist advice.</h3>
                <a href="aircraft-sale.html">Read More</a> -->
              </div>
            </div>
          </div>
        </div>
      </div>
	  <?php endwhile; wp_reset_query(); ?>
    </div>
</header>
<div class="pageContent">	
	<div class="container">
		<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; // end of the loop. ?>
	</div>
</div>

<?php get_footer(); ?>