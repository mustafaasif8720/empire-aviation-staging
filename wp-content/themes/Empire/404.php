<?php get_header(); ?>


<header class="innerheader animated fadeInLeft" style="background: url(<?php the_post_thumbnail_url(); ?>);">
	<div class="overlay">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="innercontent">
						<h2>Oops! Page not Found </h2>
			<h3>
				Get back to the <a class="custombtn btn-outline" href="<?php echo site_url() ; ?>"> Homepage</a> 
						</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>




<?php get_footer(); ?>