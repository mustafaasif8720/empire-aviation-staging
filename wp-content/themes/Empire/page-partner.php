<?php get_header(); ?>
<?php /* Template Name: Luxury Partners */ ?>

<header class="innerheader animated fadeInLeft" style="background: url(<?php the_post_thumbnail_url(); ?>);">
	<div class="overlay">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="innercontent">
					<h2><?php the_title(); ?></h2>
						<h3><?php the_field('sub_title'); ?></h3>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>




<div class="pageContent camo" style="padding-bottom: 0;">	
	<div class="container">
		<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; // end of the loop. ?>
		
		<div class="row clearBoth">
			<?php
				$args = array(
					'post_type' => 'vacations',
					'posts_per_page' => -1,
					'orderby' => 'DESC',
					'post__not_in' => array(1366,1368,1370),
			);
				$loop = new WP_Query( $args ); $i = 0;
				while ( $loop->have_posts() ) :  $loop->the_post(); global $product;
			?>
			
				<?php
				if( get_field('select_size') == 'Large' ) {
					?>
						<div class="luxurybox VacationPostItem LargeItem col-xl-8 col-lg-8 col-md-8 col-xs-12">
					<?php
				}
				else{
					?>
						<div class="luxurybox VacationPostItem SmallItem col-xl-4 col-lg-4 col-md-4 col-xs-12">
					<?php
				}
				?>
				<a href="<?php the_permalink(); ?>" style="background: url(<?php the_post_thumbnail_url(); ?>);">
					<div class="box">
						<h5>

						<?php the_title(); ?> <?php the_post_thumbnail_url(); ?> by <span><?php echo get_author_name(); ?></span> </h5>
						<h4><span><?php the_title(); ?></span> <?php the_field('sub_title'); ?> </h4>
						<p>
							<?php
								// get all product cats for the current post
								$categories = get_the_terms( get_the_ID(), 'vacations_categories' ); 

								// wrapper to hide any errors from top level categories or products without category
								if ( $categories && ! is_wp_error( $category ) ) : 

									// loop through each cat
									foreach($categories as $category) :
									  // get the children (if any) of the current cat
									  $children = get_categories( array ('taxonomy' => 'vacations_categories', 'parent' => $category->term_id ));

									  if ( count($children) == 0 ) {
										  // if no children, then echo the category name.
										  echo '#';
										  echo $category->name;
										  echo ', &nbsp;';
									  }
									endforeach;

								endif;
							?>
						</p>
					</div>
				</a>
			</div>
			<?php endwhile; wp_reset_query(); ?>
		</div>
	</div>
</div>

<style>
#three .VacationPostItem a {
	position: absolute;
	width: 92%;
}
#three .VacationPostItem .box {
	background: rgba(0,0,0,.5);
	bottom: 0;
	padding: 15px!important;
}
#three p {
	margin-bottom: 0;
}
/*Mobile*/
@media only screen and (min-width: 320px) and (max-width: 768px) {
#three .VacationPostItem .box {
	background: none;
}
#three .luxurybox {
	margin-left: 15px;
}
</style>

<!--3s-->
<div id="three" class="container" style="padding-bottom: 40px;">
	<div class="row">
		<div class="luxurybox VacationPostItem col-lg-4 col-xs-12">
			<a href="https://empireaviation.com/vacations/privateplan/" style="background: url(https://empireaviation.com/wp-content/uploads/2021/03/img-yacht.jpg) no-repeat;">
				<div class="box">
					<h5><span>Luxury Yacht Charter</span></h5>
					<h4>Absolute Privacy Aboard A Stylish Yacht</h4>
					<p>#Lifestyle #Yachtlife #Travel</p>
				</div>
			</a>
		</div>
		<div class="luxurybox VacationPostItem col-lg-4 col-xs-12">
			<a href="https://empireaviation.com/vacations/privateplan/" style="background: url(https://empireaviation.com/wp-content/uploads/2021/03/img-island.jpg) no-repeat;">
				<div class="box">
					<h5><span>Private Island</span></h5>
					<h4>Your Own Piece of Paradise on Earth</h4>
					<p>#Lifestyle #PrivateIsland #Travel</p>
				</div>
			</a>
		</div>
		<div class="luxurybox VacationPostItem col-lg-4 col-xs-12">
			<a href="https://empireaviation.com/vacations/privateplan/" style="background: url(https://empireaviation.com/wp-content/uploads/2021/03/img-horse.jpg) no-repeat;">
				<div class="box">
					<h5><span>Private Island in Europe</span></h5>
					<h4>Relax, Explore, Live the Moments to Remember</h4>
					<p>#Lifestyle #PrivateJet #Travel</p>
				</div>
			</a>
		</div>
	</div>
</div>

<div class="container grow clearBoth" id="aircrafts-sales" style="padding-bottom: 40px;">
	<?php echo do_shortcode('[contact-form-7 id="400" title="SEND US YOUR ENQUIRY"]'); ?>
</div>

<?php get_footer(); ?>