<?php get_header(); ?>
<?php /* Template Name: Aircraft Sales*/ ?>

<header class="innerheader animated fadeInLeft" style="background: url(<?php the_post_thumbnail_url(); ?>);">
	<div class="overlay">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="innercontent">
						<h2><?php the_title(); ?></h2>
						<h3><?php the_field('sub_title'); ?></h3>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>

<div class="pageContent camo">	
	<div class="container">
		<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; // end of the loop. ?>
	</div>
</div>

<!--List-->
<div class="news inventory">
	<div class="container">
	  <h2>Inventorys</h2>
	  <div class="row">
		<?php $args = array(
		'post_type' => 'aircraft',
		'posts_per_page' => 6,
		'orderby' => 'date',
		'order' => 'DESC',
		);
		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post(); ?>
	    <div class="col-xl-4">
	      <div class="newsbox">
	        <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" />
	        <h4><?php the_title(); ?></h4>
	        <p><?php the_content(); ?></p>
	        <a href="<?php the_permalink(); ?>" target="_blank">Read More</a>
	      </div>
	    </div>
		<?php endwhile; ?>
		<?php $wp_query = null; $wp_query = $temp;?>
		<?php wp_reset_query(); ?>
	  </div>
	  <!-- <div align="center">
	    <a href="#aircrafts-sales" class="custombtn btn-outline btnmrg">View All</a>
	  </div> -->
	</div>
</div>

<div class="container grow" id="aircrafts-sales">
	<?php echo do_shortcode('[contact-form-7 id="782" title="AIRCRAFT SALES"]'); ?>
</div>

<?php get_footer(); ?>