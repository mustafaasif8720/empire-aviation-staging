<?php get_header(); ?>
<?php /* Template Name: Bottom Charter Form */ ?>

<header class="innerheader animated fadeInLeft" style="background: url(<?php the_post_thumbnail_url(); ?>);">
	<div class="overlay">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="innercontent">
						<h2><?php the_title(); ?></h2>
						<h3><?php the_field('sub_title'); ?></h3>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>

<div class="pageContent camo">	
	<div class="container">
		<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; // end of the loop. ?>
	</div>
</div>

<div class="container grow" id="aircrafts-sales">
	<?php echo do_shortcode('[contact-form-7 id="227" title="Charter Inquiry Form"]'); ?>
</div>

<?php get_footer(); ?>