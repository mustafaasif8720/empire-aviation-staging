<?php get_header(); ?>

<header class="innerheader animated fadeInLeft slide-news">
	<div class="overlay">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="innercontent">
						<h2><?php echo get_the_category( $id )[0]->name; ?> </h2>
						
						<a href="<?php echo get_site_url(); ?>/news/">Read More</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>

<div class="container news-inner">
	<div class="row">
		<div class="col-xl-12 col-lg-12 col-md-12 col-xs-12">
			<br/><br/>
			<?php if (get_field('video') != "") { ?>
				<div class="embed-responsive embed-responsive-16by9">
					<?php the_field('video'); ?>
				</div>
			<?php } else if (has_post_thumbnail()) { ?>
				<img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" width="500" align="left" style="margin-right: 20px; margin-bottom: 10px;"/>
			<?php } else { ?>
				<img src="https://empireaviation.com/wp-content/uploads/2021/03/default-news2.jpg" alt="<?php the_title(); ?>" width="500" align="left" style="margin-right: 20px; margin-bottom: 10px;"/>
			<?php } ?>
			<h2><strong>
				<?php echo the_title(); ?>
				</strong>
			</h2>
			<br>
			<?php echo the_content(); ?>
		</div>
	</div>
	
	<div class="row">
		<?php echo do_shortcode('[contact-form-7 id="392" title="Single Post"]'); ?>
	</div>
</div>


<?php get_footer(); ?>