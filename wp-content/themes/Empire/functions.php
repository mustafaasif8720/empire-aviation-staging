<?php

//Excerpt
function custom_excerpt_length( $length ) {
    return 15;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
function new_excerpt_more( $more ) {
    return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

//Logo
add_theme_support( 'custom-logo', array(
	'height'      => 100,
	'width'       => 400,
	'flex-height' => true,
	'flex-width'  => true,
	'header-text' => array( 'site-title', 'site-description' ),
));

//Menu
function daily_menus() {
  register_nav_menus(
    array(
      'primary-menu' => __( 'Primary Menu' ),
      'secoundry-menu' => __( 'Secoundry Menu' )
    )
  );
}
add_action( 'init', 'daily_menus' );

add_theme_support( 'post-thumbnails' ); 
// without parameter -> Post Thumbnail (as set by theme using set_post_thumbnail_size())
the_post_thumbnail();
the_post_thumbnail('thumbnail');       // Thumbnail (default 150px x 150px max)
the_post_thumbnail('medium');          // Medium resolution (default 300px x 300px max)
the_post_thumbnail('large');           // Large resolution (default 640px x 640px max)
the_post_thumbnail('full');            // Original image resolution (unmodified)
the_post_thumbnail( array(100,100) );  // Other resolutions

function daily_widgets() {
	register_sidebar( array(
		'name' => __( 'Copy Right', 'twentyten' ),
		'id' => 'Copy-Right',
		'description' => __( 'Copy Right', 'twentyten' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s Copy-Right">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));
	register_sidebar( array(
		'name' => __( 'Footer 1', 'twentyten' ),
		'id' => 'Footer-1',
		'description' => __( 'Footer 1', 'twentyten' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s Footer-1">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));
	register_sidebar( array(
		'name' => __( 'Footer 2', 'twentyten' ),
		'id' => 'Footer-2',
		'description' => __( 'Footer 2', 'twentyten' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s Footer-2">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));
}
add_action( 'widgets_init', 'daily_widgets' );

//Slider
function slider_register() {
    $labels = array(
        'name' => _x('Slider', 'post type general name'),
        'singular_name' => _x('Add slider', 'post type singular name'),
        'add_new' => _x('Add New', 'Add slider'),
        'add_new_item' => __('Add slider'),
        'edit_item' => __('Edit Add slider'),
        'new_item' => __('New Add slider'),
        'view_item' => __('View Add slider'),
        'search_items' => __('Search Add slider'),
        'not_found' =>  __('Nothing found'),
        'not_found_in_trash' => __('Nothing found in Trash'),
        'parent_item_colon' => ''
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => 8,
        'supports' => array('title','editor','thumbnail')
    ); 
    register_post_type( 'slider' , $args );
}
add_action('init', 'slider_register');

function slider_taxonomies() {
    $labels = array(
        'name'              => _x( 'Categories', 'taxonomy general name' ),
        'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Categories' ),
        'all_items'         => __( 'All Categories' ),
        'parent_item'       => __( 'Parent Category' ),
        'parent_item_colon' => __( 'Parent Category:' ),
        'edit_item'         => __( 'Edit Category' ),
        'update_item'       => __( 'Update Category' ),
        'add_new_item'      => __( 'Add New Category' ),
        'new_item_name'     => __( 'New Category Name' ),
        'menu_name'         => __( 'Categories' ),
    );

    $args = array(
        'hierarchical'      => true, //Set this to 'false' for non-hierarchical taxonomy (like tags)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'categories' ),
    );
    register_taxonomy( 'slider_categories', array( 'slider' ), $args );
}
add_action( 'init', 'slider_taxonomies', 0 );

//Magazine
function magazine_register() {
    $labels = array(
        'name' => _x('Magazine', 'post type general name'),
        'singular_name' => _x('Add magazine', 'post type singular name'),
        'add_new' => _x('Add New', 'Add magazine'),
        'add_new_item' => __('Add magazine'),
        'edit_item' => __('Edit Add magazine'),
        'new_item' => __('New Add magazine'),
        'view_item' => __('View Add magazine'),
        'search_items' => __('Search Add magazine'),
        'not_found' =>  __('Nothing found'),
        'not_found_in_trash' => __('Nothing found in Trash'),
        'parent_item_colon' => ''
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => 8,
        'supports' => array('title','editor','thumbnail')
    ); 
    register_post_type( 'magazine' , $args );
}
add_action('init', 'magazine_register');

function magazine_taxonomies() {
    $labels = array(
        'name'              => _x( 'Categories', 'taxonomy general name' ),
        'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Categories' ),
        'all_items'         => __( 'All Categories' ),
        'parent_item'       => __( 'Parent Category' ),
        'parent_item_colon' => __( 'Parent Category:' ),
        'edit_item'         => __( 'Edit Category' ),
        'update_item'       => __( 'Update Category' ),
        'add_new_item'      => __( 'Add New Category' ),
        'new_item_name'     => __( 'New Category Name' ),
        'menu_name'         => __( 'Categories' ),
    );

    $args = array(
        'hierarchical'      => true, //Set this to 'false' for non-hierarchical taxonomy (like tags)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'categories' ),
    );
    register_taxonomy( 'magazine_categories', array( 'magazine' ), $args );
}
add_action( 'init', 'magazine_taxonomies', 0 );

//Vacations
function vacations_register() {
    $labels = array(
        'name' => _x('Vacations', 'post type general name'),
        'singular_name' => _x('Add vacations', 'post type singular name'),
        'add_new' => _x('Add New', 'Add vacations'),
        'add_new_item' => __('Add vacations'),
        'edit_item' => __('Edit Add vacations'),
        'new_item' => __('New Add vacations'),
        'view_item' => __('View Add vacations'),
        'search_items' => __('Search Add vacations'),
        'not_found' =>  __('Nothing found'),
        'not_found_in_trash' => __('Nothing found in Trash'),
        'parent_item_colon' => ''
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => 8,
        'supports' => array('title','editor','thumbnail')
    ); 
    register_post_type( 'vacations' , $args );
}
add_action('init', 'vacations_register');

function vacations_taxonomies() {
    $labels = array(
        'name'              => _x( 'Categories', 'taxonomy general name' ),
        'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Categories' ),
        'all_items'         => __( 'All Categories' ),
        'parent_item'       => __( 'Parent Category' ),
        'parent_item_colon' => __( 'Parent Category:' ),
        'edit_item'         => __( 'Edit Category' ),
        'update_item'       => __( 'Update Category' ),
        'add_new_item'      => __( 'Add New Category' ),
        'new_item_name'     => __( 'New Category Name' ),
        'menu_name'         => __( 'Categories' ),
    );

    $args = array(
        'hierarchical'      => true, //Set this to 'false' for non-hierarchical taxonomy (like tags)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'categories' ),
    );
    register_taxonomy( 'vacations_categories', array( 'vacations' ), $args );
}
add_action( 'init', 'vacations_taxonomies', 0 );

//Awards
function awards_register() {
    $labels = array(
        'name' => _x('Award', 'post type general name'),
        'singular_name' => _x('Add Award', 'post type singular name'),
        'add_new' => _x('Add New', 'Add Award'),
        'add_new_item' => __('Add Award'),
        'edit_item' => __('Edit Add Award'),
        'new_item' => __('New Add Award'),
        'view_item' => __('View Add Award'),
        'search_items' => __('Search Add Award'),
        'not_found' =>  __('Nothing found'),
        'not_found_in_trash' => __('Nothing found in Trash'),
        'parent_item_colon' => ''
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => 8,
        'supports' => array('title','editor','thumbnail')
    ); 
    register_post_type( 'awards' , $args );
}
add_action('init', 'awards_register');

function awards_taxonomies() {
    $labels = array(
        'name'              => _x( 'Categories', 'taxonomy general name' ),
        'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Categories' ),
        'all_items'         => __( 'All Categories' ),
        'parent_item'       => __( 'Parent Category' ),
        'parent_item_colon' => __( 'Parent Category:' ),
        'edit_item'         => __( 'Edit Category' ),
        'update_item'       => __( 'Update Category' ),
        'add_new_item'      => __( 'Add New Category' ),
        'new_item_name'     => __( 'New Category Name' ),
        'menu_name'         => __( 'Categories' ),
    );

    $args = array(
        'hierarchical'      => true, //Set this to 'false' for non-hierarchical taxonomy (like tags)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'categories' ),
    );
    register_taxonomy( 'awards_categories', array( 'awards' ), $args );
}
add_action( 'init', 'awards_taxonomies', 0 );

//Story
function story_register() {
    $labels = array(
        'name' => _x('Story', 'post type general name'),
        'singular_name' => _x('Add Story', 'post type singular name'),
        'add_new' => _x('Add New', 'Add story'),
        'add_new_item' => __('Add story'),
        'edit_item' => __('Edit Add story'),
        'new_item' => __('New Add story'),
        'view_item' => __('View Add story'),
        'search_items' => __('Search Add story'),
        'not_found' =>  __('Nothing found'),
        'not_found_in_trash' => __('Nothing found in Trash'),
        'parent_item_colon' => ''
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => 8,
        'supports' => array('title','editor','thumbnail')
    ); 
    register_post_type( 'story' , $args );
}
add_action('init', 'story_register');

function story_taxonomies() {
    $labels = array(
        'name'              => _x( 'Categories', 'taxonomy general name' ),
        'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Categories' ),
        'all_items'         => __( 'All Categories' ),
        'parent_item'       => __( 'Parent Category' ),
        'parent_item_colon' => __( 'Parent Category:' ),
        'edit_item'         => __( 'Edit Category' ),
        'update_item'       => __( 'Update Category' ),
        'add_new_item'      => __( 'Add New Category' ),
        'new_item_name'     => __( 'New Category Name' ),
        'menu_name'         => __( 'Categories' ),
    );

    $args = array(
        'hierarchical'      => true, //Set this to 'false' for non-hierarchical taxonomy (like tags)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'categories' ),
    );
    register_taxonomy( 'story_categories', array( 'story' ), $args );
}
add_action( 'init', 'story_taxonomies', 0 );

//Aircraft
function aircraft_register() {
    $labels = array(
        'name' => _x('Aircraft', 'post type general name'),
        'singular_name' => _x('Add aircraft', 'post type singular name'),
        'add_new' => _x('Add New', 'Add aircraft'),
        'add_new_item' => __('Add aircraft'),
        'edit_item' => __('Edit Add aircraft'),
        'new_item' => __('New Add aircraft'),
        'view_item' => __('View Add aircraft'),
        'search_items' => __('Search Add aircraft'),
        'not_found' =>  __('Nothing found'),
        'not_found_in_trash' => __('Nothing found in Trash'),
        'parent_item_colon' => ''
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => 8,
        'supports' => array('title','editor','thumbnail')
    ); 
    register_post_type( 'aircraft' , $args );
}
add_action('init', 'aircraft_register');

function aircraft_taxonomies() {
    $labels = array(
        'name'              => _x( 'Categories', 'taxonomy general name' ),
        'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Categories' ),
        'all_items'         => __( 'All Categories' ),
        'parent_item'       => __( 'Parent Category' ),
        'parent_item_colon' => __( 'Parent Category:' ),
        'edit_item'         => __( 'Edit Category' ),
        'update_item'       => __( 'Update Category' ),
        'add_new_item'      => __( 'Add New Category' ),
        'new_item_name'     => __( 'New Category Name' ),
        'menu_name'         => __( 'Categories' ),
    );

    $args = array(
        'hierarchical'      => true, //Set this to 'false' for non-hierarchical taxonomy (like tags)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'categories' ),
    );
    register_taxonomy( 'aircraft_categories', array( 'aircraft' ), $args );
}
add_action( 'init', 'magazine_taxonomies', 0 );



//Magazine
function shortcode_Magazine() {
  ?>
	<ul class="clogo">
		<?php
			$args = array( 'post_type' => 'magazine', 'posts_per_page' => -1,'orderby' => 'DESC' );
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post(); global $product;
		?>
		<li>
		  <a href="<?php the_field('link'); ?>" target="_blank">
			<img src="<?php the_post_thumbnail_url(); ?>" alt=""> </a>
		  <p><?php the_title(); ?></p>
		</li>
		<?php endwhile; wp_reset_query(); ?>
	</ul>
  <?php
}
add_shortcode('Magazine', 'shortcode_Magazine');

//Vacations
function shortcode_vacations() {
  ?>
<ul class="dlogo vactionsSlider">
	<?php
		$args = array( 'post_type' => 'vacations', 'posts_per_page' => -1,'orderby' => 'DESC' );
		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post(); global $product;
	?>
	<li>
        <?php if (get_field('custom_link')=="") { ?>
	      <a href="<?php the_permalink(); ?>"  class="image-wrap">
        <?php } else { ?>
          <a href="<?php the_field('custom_link'); ?>"  class="image-wrap">
        <?php } ?>
    	  <img src="<?php the_field('add_front_image'); ?>" width="298" height="360" alt="" />
    	  <div class="box">
    		<h5><?php the_title(); ?></h5>
    		<h4><?php //the_title(); ?> <?php the_field('sub_title'); ?></h4>
    		<p>
    			<?php
    			$categories = get_the_terms( get_the_ID(), 'vacations_categories' );
    			if ( $categories && ! is_wp_error( $category ) ) : 
    				foreach($categories as $category) :
    				  $children = get_categories( array ('taxonomy' => 'vacations_categories', 'parent' => $category->term_id ));
    				  if ( count($children) == 0 ) {
    					  echo '#';
    					  echo $category->name;
    					  echo ', &nbsp;';
    				  }
    				endforeach;
    			endif;
    			?>
    		</p>
    	  </div>
	    </a>
	</li>
	<?php endwhile; wp_reset_query(); ?>
</ul>
  <?php
}
add_shortcode('Vacations', 'shortcode_vacations');

//News
function shortcode_news() {
	?>
<div class="row">
	<?php
		$args = array( 'post_type' => 'post', 'posts_per_page' => 3,'orderby' => 'DESC' );
		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post(); global $product;
	?>
	<div class="col-xl-4">
		<div class="newsarea">
			<span class="ThumbnailImg" style="background: url(<?php the_post_thumbnail_url(); ?>);"></span>
			<span class="date"><?php echo get_the_date(); ?></span>
			<h4><?php the_title(); ?></h4>
			<p>
				<?php the_field('small_content'); ?>
			</p>
			<a href="<?php the_permalink(); ?>" class="">Read More</a>
		</div>
	</div>
	<?php endwhile; wp_reset_query(); ?>
</div>
	<?php
}
add_shortcode('News', 'shortcode_news');

//Awards
function shortcode_awards() {
	?>
<div class="row">
	<?php
		$args = array( 'post_type' => 'awards', 'posts_per_page' => 4,'orderby' => 'DESC' );
		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post(); global $product;
	?>
<div class="col-xl-3">
	<div class="flip-card">
		<div class="flip-card-inner">
			<div class="flip-card-front">
				<div>
					<img src="<?php the_post_thumbnail_url(); ?>" alt="Award Winning">
				</div>
			</div>
			<div class="flip-card-back">
				<!-- <h5 class="text-uppercase"><?php //the_title(); ?></h5> -->
				<p><?php the_content(); ?></p>
			</div>
		</div>
	</div> 
</div>
	<?php endwhile; wp_reset_query(); ?>
</div>
	<?php
}
add_shortcode('Awards', 'shortcode_awards');

//Awards2
function shortcode_Aws() {
  ?>
    <ul id="aws" class="elogo">
        <?php
            $args = array( 'post_type' => 'awards', 'posts_per_page' => -1,'orderby' => 'DESC' );
            $loop = new WP_Query( $args );
            while ( $loop->have_posts() ) : $loop->the_post(); global $product;
        ?>
        <li style="margin:0 15px;">
        <div class="flip-card">
            <div class="flip-card-inner">
                <div class="flip-card-front">
                    <div>
                        <img src="<?php the_post_thumbnail_url(); ?>" alt="Award Winning">
                    </div>
                </div>
                <div class="flip-card-back">
                    <!-- <h5 class="text-uppercase"><?php //the_title(); ?></h5> -->
                    <p><?php the_content(); ?></p>
                </div>
            </div>
        </div>
        </li>
        <?php endwhile; wp_reset_query(); ?>
    </ul>
  <?php
}
add_shortcode('Aws', 'shortcode_Aws');

/*
vc_add_param("vc_column", array(
    "type" => "vc_link",
	"class" => "",
	"heading" => "Column Link",
	"param_name" => "column_link"
));
vc_add_param("vc_column_inner", array(
    "type" => "vc_link",
	"class" => "",
	"heading" => "Column Link",
	"param_name" => "column_link"
));*/