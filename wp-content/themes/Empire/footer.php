<div class="footer  animated fadeInUp grow">
	<a onclick="topFunction()" class="ltx-go-top floating show" id="gotop" title="Go to top">
		<img src="<?php bloginfo('template_directory'); ?>/images/go-top.png" class="attachment-full size-full" alt="" loading="lazy" width="20" height="19">
		<span>Go Top</span>
	</a>
	<div class="container">
		<div class="row">
			<div class="col-xl-4">
				<?php if (get_theme_mod( 'custom_logo' )) { ?>
					<img src="<?php $custom_logo_id = get_theme_mod( 'custom_logo' ); $image = wp_get_attachment_image_src( $custom_logo_id , 'full' ); echo $image[0]; ?> " />
				<?php } else { ?>
					<?php bloginfo( 'name' ); ?>
				<?php } ?>
				<?php dynamic_sidebar( 'Copy-Right' ); ?>
			</div>
			<div class="col-xl-6">
				<?php dynamic_sidebar( 'Footer-1' ); ?>
			</div>
			<div class="col-xl-2">
        <?php dynamic_sidebar( 'Footer-2' ); ?>   
      </div>
		</div>
	</div>
</div>

<!--Fleet Guide-->
<div class="modal fade" id="modal-agreement">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
    	<div class="modal-body">
    	  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    		<span aria-hidden="true">&times;</span>
    	  </button>
    	  <embed src="<?php the_field('pdf'); ?>" frameborder="0" width="100%" height="600px">
    	  <!-- <object type="application/pdf" data="images/fleet-guide/empire-aviation-fleet-guide-01-2020-2.pdf" width="100%" height="500" style="height: 85vh;">No Support</object> -->
    	</div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 

<!--What???-->
<div id="booknow" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-12">
            <h3>GET IN TOUCH</h3>
            <div class="row">
              <div class="col-lg-6">
                <input type="text" placeholder="First Name*">
              </div>
              <div class="col-lg-6">
                <input type="text" placeholder="last Name*">
              </div>
              <div class="col-lg-6">
                <input type="text" placeholder="Email*">
              </div>
              <div class="col-lg-6">
                <input type="text" placeholder="Phone Number*">
              </div>
              <div class="col-lg-12">
                <textarea rows="5" placeholder="Message in the box"></textarea>
              </div>
              <div class="col-lg-12">
                <input type="button" value="Submit">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!--Book Now-->
<div id="requestnow" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
	      <h5 class="modal-title">Book A Charter</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <?php echo do_shortcode('[contact-form-7 id="623" title="Book Now"]'); ?>
      </div>
    </div>
  </div>
</div>

<!--Covid-->
<div id="emailPopup" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-12">
            <h3 class="text-center">EMPIRE AVIATION OPERATIONS RESPONSE TO COVID-19</h3>
            <p class="py-4 text-center">As we navigate this pandemic, we are continually updating our COVID-19
              protocols, to comply with regional and international safety guidelines for private aircraft operations.
            </p>
            <p class="text-center">Please feel free to contact us with any questions or concerns.</p>
            <div class="row pl-3">
              <div class="col-6">
                <p>General enquiries:</p>
                <p><a href="tel:+97142998444"><i class="fa fa-phone"></i> +971 4 299 8444 </a> </p>
                <p><a href="mailto:info@empire.aero"><i class="fa fa-envelope"></i>info@empire.aero</a></p>
              </div>
              <div class="col-6">
                <p>Aircraft charters services:</p>
                <p><a href="tel:+97142998444"><i class="fa fa-phone"></i> +971 4 299 8444 </a> </p>
                <p><a href="tel:+971565091116"><i class="fa fa-phone"></i> +971 56 509 1116 </a> </p>
                <p><a href="mailto:charter@empire.aero"><i class="fa fa-envelope"></i>charter@empire.aero</a></p>
              </div>
            </div>
            <div class="modal-footer">
              <p class="text-center">FOR THE LATEST INFORMATION ON COVID-19 GLOBAL TRAVEL REGULATIONS, <a
                  href="https://www.iatatravelcentre.com/international-travel-document-news/1580226297.htm"
                  target="_blank"> CLICK HERE </a> TO VIEW THE IATA INTERACTIVE MAP.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php wp_footer(); ?>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery-1.12.4.js"></script>
<!--script src="js/jquery.slim.min.js"></script-->
<script src="<?php bloginfo('template_directory'); ?>/js/bootstrap.bundle.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/slick.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/css/fancybox/source/jquery.fancybox.js"></script>

<script>
	$(document).ready(function () {
      //   $(window).on('load', function() {
      //     $('#emailPopup').modal('show');
      // });
      $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
      });
      $('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: true,
        centerMode: false,
        focusOnSelect: true
      });
      $('.mainslider').slick({
        dots: false,
        arrows: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }

        ]
      });
      $('.clogo').slick({
        dots: false,
		    autoplay:true,
        arrows: true,
        infinite: true,
        speed: 1000,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }

        ]
      });
      $('.elogo').slick({
        dots: false,
        autoplay:false,
        arrows: true,
        infinite: true,
        speed: 1000,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }

        ]
      });
      $('.dlogo').slick({
        dots: false,
        arrows: true,
        infinite: true,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }

        ]
      });
	 $('.gallery-slick').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        autoplay: true,
        dots: true,
      });

    });
</script>

<script>
//Get the button:
mybutton = document.getElementById("gotop");
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function () { scrollFunction() };
function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
	mybutton.style.display = "inline-grid";
  } else {
	mybutton.style.display = "none";
  }
}
// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}
</script>

<script type="text/javascript">
$(document).ready(function () {
  /*
   *  Simple image gallery. Uses default settings
   */
  $('.fancybox').fancybox();

  /*
   *  Different effects
   */
  // Change title type, overlay closing speed
  $(".fancybox-effects-a").fancybox({
	helpers: {
	  title: {
		type: 'outside'
	  },
	  overlay: {
		speedOut: 0
	  }
	}
  });
  // Disable opening and closing animations, change title type
  $(".fancybox-effects-b").fancybox({
	openEffect: 'none',
	closeEffect: 'none',

	helpers: {
	  title: {
		type: 'over'
	  }
	}
  });
  // Set custom style, close if clicked, change title type and overlay color
  $(".fancybox-effects-c").fancybox({
	wrapCSS: 'fancybox-custom',
	closeClick: true,
	openEffect: 'none',
	helpers: {
	  title: {
		type: 'inside'
	  },
	  overlay: {
		css: {
		  'background': 'rgba(238,238,238,0.85)'
		}
	  }
	}
  });
  // Remove padding, set opening and closing animations, close if clicked and disable overlay
  $(".fancybox-effects-d").fancybox({
	padding: 0,
	openEffect: 'elastic',
	openSpeed: 150,
	closeEffect: 'elastic',
	closeSpeed: 150,
	closeClick: true,
	helpers: {
	  overlay: null
	}
  });
  /*
   *  Button helper. Disable animations, hide close button, change title type and content
   */
  $('.fancybox-buttons').fancybox({
	openEffect: 'none',
	closeEffect: 'none',
	prevEffect: 'none',
	nextEffect: 'none',
	closeBtn: false,
	helpers: {
	  title: {
		type: 'inside'
	  },
	  buttons: {}
	},
	afterLoad: function () {
	  this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
	}
  });
});
	
</script>

</body>
</html>