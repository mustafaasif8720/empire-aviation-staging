<?php get_header(); ?>

<?php
/*
 * Template Name: Multi
 * Template Post Type: vacations
 */
  
 get_header();  ?>


<?php 
if ( have_posts() ) : 
    while ( have_posts() ) : the_post(); 
        ?>
		
<header class="innerheader animated fadeInLeft soneva-kiri">
	<video autoplay muted loop id="bgVideo">
		<source src="<?php the_field('video'); ?>" type="video/mp4">
	</video>
	
	<div class="container">
		<div class="sub-container">
			<div class="row">
				<div class="col-lg-12">
					<div class="innercontent">
						<?php if( get_field('logo') ): ?>
							<img src="<?php the_field('logo'); ?>" alt="">
						<?php endif; ?>
						<?php if( get_field('sub_title') ): ?>
							<h3 class="luxury-heading"><?php the_field('sub_title'); ?></h3>
						<?php endif; ?>
						<?php if( get_field('sub_content') ): ?>
							<h3><?php the_field('sub_content'); ?></h3>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</header>


<div class="container">

    <div class="row">
      <div class="col-xl-9 col-lg-8 col-md-12 col-12 pt-4 text-center luxury-panel animated zoomIn">
		<?php if( get_field('logo') ): ?>
			<img src="<?php the_field('logo'); ?>" alt="">
		<?php endif; ?>
        <?php if( get_field('sub_title') ): ?>
			<h3 class="luxury-heading"><?php the_field('sub_title'); ?></h3>
		<?php endif; ?>
        <h2>About <?php the_title(); ?></h2>
        <?php the_content(); ?>
      </div>
    </div>


<div class="row my-5 imageswipe animated d-none d-md-flex sf zoomIn">
      <div class="col-md-12">
        <div class="row collage-photos">
<?php
	if( have_rows('gallery') ):
	while ( have_rows('gallery') ) : the_row();
?>
          <div class="col-md-3 mb-3">
            <a class="fancybox cuxtomSize" href="<?php the_sub_field('add_image'); ?>" data-fancybox-group="gallery" title="" style="background: url(<?php the_sub_field('add_image'); ?>);"></a>
          </div>
<?php endwhile; endif; ?>
        </div>
      </div>
    </div>

    <div class="row my-5 imageswipe animated d-block d-md-none zoomIn">
      <div class="col-12 gallery-slick">
<?php
	if( have_rows('gallery') ):
	while ( have_rows('gallery') ) : the_row();
?>
	<div><img src="<?php the_sub_field('add_image'); ?>" alt="" /></div>
<?php endwhile; endif; ?>
      </div>
    </div>
	
	<div class="onlineform luxury-panel">
      <h3 class="text-center">Fill the below form for information</h3>
      <h2><?php the_field('form_title'); ?></h2>
	  <?php echo do_shortcode('[contact-form-7 id="392" title="Single Post"]'); ?>
    </div>



  </div>


		<?php
    endwhile; 
endif; 
?>	



<?php get_footer(); ?>