<!doctype html>
<html>
<head>

	<!--Meta Tags-->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <!--Meta Tags End-->
  
  <!--Favi Icon-->
  <link rel="icon" href="<?php echo get_site_icon_url(); ?>" type="image/png" sizes="16x16">
  <!--Favi Icon End-->
    
	<!--Title-->
  <title>
  <?php
      /** Print the <title> tag based on what is being viewed. */
      global $page, $paged;
      wp_title( '|', true, 'right' );
      // Add the blog name.
      bloginfo( 'name' );
      // Add the blog description for the home/front page.
      $site_description = get_bloginfo( 'description', 'display' );
      if ( $site_description && ( is_home() || is_front_page() ) )
          echo " | $site_description";
      // Add a page number if necessary:
      if ( $paged >= 2 || $page >= 2 )
      echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );
  ?>
  </title>
  <!--Title-->
    
	<!--StyleSheet-->
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/style.css" />
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900&display=swap" rel="stylesheet">
	<link href="<?php bloginfo('template_directory'); ?>/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php bloginfo('template_directory'); ?>/css/slick.css" rel="stylesheet">
	<!--link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"-->
	<link href="<?php bloginfo('template_directory'); ?>/css/custom.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
	integrity="sha512-SfTiTlX6kk+qitfevl/7LibUOeJWlt9rbyDn92a1DqWOw9vWG2MFoays0sgObmWazO5BQPiFucnnEAjpAB+/Sw=="
	crossorigin="anonymous" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.compat.css" />
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/fancybox/source/jquery.fancybox.css?v=2.1.5" media="screen" />

	
  <!-- Google Tag Manager
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-NSM3V7');</script>
  End Google Tag Manager -->

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5WMJ729');</script>
<!-- End Google Tag Manager -->
<?php wp_head(); ?>

	</head>
<body <?php body_class( $class ); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5WMJ729"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- Google Tag Manager (noscript) 
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NSM3V7"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<section class="d-none d-md-block topbar">
  <div class="container">
    <div class="row pt-3">
      <div class="col-md-6 text-left">
      </div>
      <div class="col-md-6 text-right text-white">
        <i class="fa fa-phone"></i> <a href="tel:+97142998444" class="text-white mr-4"> + 971 4 299 8444 </a>
        <i class="fa fa-envelope"></i> <a href="mailto:info@empire.aero" class="text-white mr-4"> info@empire.aero
        </a>
      </div>
    </div>
  </div>
</section>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark static-top animated fadeInDown">
  <div class="container">
  	<?php if (get_theme_mod( 'custom_logo' )) { ?>
  		<a href="<?php echo get_site_url(); ?>" class="navbar-brand">
  			<img src="<?php $custom_logo_id = get_theme_mod( 'custom_logo' ); $image = wp_get_attachment_image_src( $custom_logo_id , 'full' ); echo $image[0]; ?> " />
  		</a>
  	<?php } else { ?>
  		<a href="<?php echo get_site_url(); ?>" class="navbar-brand"><?php bloginfo( 'name' ); ?></a>
  	<?php } ?>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
      aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
    	<?php wp_nav_menu( array( 'theme_location' => 'primary-menu' ) ); ?>
    	<button class="bookNowBtn" data-toggle="modal" onclick="ga('send', 'event', 'Lead', 'BookNow', 'BookCharter');" data-target="#requestnow">Book Now</button>
	  </div>
  </div>
</nav>

<style>
.popdestination .box {
  padding: 18px!important;
}
.popdestination .box p {
  margin-bottom: 0;
}
</style>